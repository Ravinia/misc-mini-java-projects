public class LinkedList {

	public Node root;
	public Node end;
	public int size = 0;
	
	public void insert(Node node)
	{
		if (end == null)
		{
			root = node;
			end = node;
			return;
		}
		end.next = node;
		end = node;
	}
	
	public static void main(String args[])
	{
		
	}
	
}
