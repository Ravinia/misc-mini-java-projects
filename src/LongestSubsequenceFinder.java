
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Finds the longest subsequence in an array that adds up to a target value 
 * @author Jameson Saunders
 */
public class LongestSubsequenceFinder
{
	Logger logger;
	
	public LongestSubsequenceFinder()
	{
		logger = Logger.getLogger("LongestSubsequenceSolver");
		logger.setLevel(Level.OFF);
	}
	
	/**
	 * O(n^2) Finds the longest subsequence in nums which sums to target.
	 * @param nums The array to find the longest subsequence in.
	 * @param target The number which the longest subsequence must sum to.
	 * @return The length of the longest subsequence which sums to target.
	 */
	public int find1(int nums[], int target)
	{
		int largest = 0;
		
		for (int i=0; i<nums.length; i++)
		{
			int currentSum = 0;
			for (int j=i; j<nums.length; j++)
			{
				currentSum += nums[j];
				if (currentSum == target && j-i+1 > largest)
					largest = j-i+1;
			}
		}
		
		return largest;
	}
	
	/**
	 * O(n) Finds the longest subsequence in nums which sums to target.
	 * @param nums The array to find the longest subsequence in.
	 * @param target The number which the longest subsequence must sum to.
	 * @return The length of the longest subsequence which sums to target.
	 */
	public int find2(int nums[], int target)
	{
		int largest = 0;
		
		int currentSum = 0;
		HashMap<Integer,TreeSet<Integer>> valuesToIndecies = new HashMap<Integer, TreeSet<Integer>>();
		for (int i=0; i<nums.length; i++)
		{
			currentSum += nums[i];
			logger.info("currentSum: "+currentSum);
			
			int counterValue = currentSum-target;
			logger.info("counterValue: "+counterValue);
			
			// insert currentSum into the map
			TreeSet<Integer> s;
			if (!valuesToIndecies.containsKey(currentSum))
			{
				s = new TreeSet<Integer>();
				valuesToIndecies.put(currentSum, s);
			}
			else
				s = valuesToIndecies.get(currentSum);
			s.add(i);
			
			// adjust largest if counter value exists
			if (valuesToIndecies.containsKey(counterValue))
			{
				s = valuesToIndecies.get(counterValue);
				logger.info("set: "+s);
				
				int counterIndex = s.first();
				logger.info("counterIndex: "+counterIndex);
				int prospect = i-counterIndex;
				if (prospect > largest)
					largest = prospect;
			}
		}
		
		return largest;
	}
	
	public static void main(String args[])
	{
		LongestSubsequenceFinder lss = new LongestSubsequenceFinder();
		
		int nums[] = {1,-2,3,7,4};
		int target = 10;
		System.out.println("nums: " + Arrays.toString(nums));
		System.out.println("target: "+target);
		System.out.println("solution: " + lss.find1(nums, target));
		System.out.println("solution: " + lss.find2(nums, target)+'\n');
		
		int nums2[] = {4,4,-3,3,2,1,5,-3,-8,2};
		int target2 = 3;
		System.out.println("nums: " + Arrays.toString(nums2));
		System.out.println("target: "+target2);
		System.out.println("solution: " + lss.find1(nums2, target2));
		System.out.println("solution2: " + lss.find2(nums2, target2)+'\n');
		
		int nums3[] = {-1,1,3,5,-4,-3,4,6,-2,-4,5,3,1,1,1,1,-1,-1,-1,-1,-1,10,-21,15, 0, 1};
		int target3 = 0;
		System.out.println("nums: " + Arrays.toString(nums3));
		System.out.println("target: "+target3);
		System.out.println("solution: " + lss.find1(nums3, target3));
		System.out.println("solution2: " + lss.find2(nums3, target3)+'\n');
	}
}
