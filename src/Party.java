import java.util.ArrayList;

public class Party
{
	// instance variables that will hold your data
	// private indicates they belong to this class exclusively
	private int maxGuests;
	private String host;
	private ArrayList<String> guests;
	
	//Constructor
	public Party(int maxGuests, String host)
	{
		System.out.println("Initializing party with maxGuests: '" + maxGuests + "' and host: '" + host + "'");
		this.guests = new ArrayList<String>();
		this.maxGuests = maxGuests;
		this.host = host;
	}
	
	//getter
	// define type of data to be returned
	public String getHost()
	{
		System.out.println("Setting host to: " + host);
		return host;
	}
	
	//setter
	// setters have type void b/c they return nothing
	public void setHost(String host)
	{
		System.out.println("Setting host to: " + host);
		this.host = host;
	}
	
	//*************************************
	//Method to add to guest list
	public void addGuest(String guest)
	{
		if (guests.size() < maxGuests)
		{
			System.out.println("Adding guest: " + guest);
			this.guests.add(guest);
		}
		else
		{
			System.out.println("Guest list full");
		}
	}
	
	//*************************************
	//Method to print party
	public void printParty()
	{
		System.out.println("Guest list for " +
		this.host + "'s party is: \n\n" +
		this.guests + ".\n");
	} // end Print Party method
	
	public static void main(String[] args)
	{
		
	  Party party = new Party(3, "David Beckham");
	  party.addGuest("Roberto Baggio");
	  party.addGuest("Zinedine Zidane");
	  party.addGuest("Roberto Baggio");
	  party.addGuest("Johan Cruyff");
	  party.addGuest("Diego Maradona");
	  party.printParty();

	} // end main

}//end class Party