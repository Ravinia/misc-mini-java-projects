import java.util.Stack;

@SuppressWarnings({ "serial" })
public class MaxStack<E extends Comparable<E>> extends Stack<E>
{
	private Stack<E> stack = new Stack<E>();
	private Stack<E> maxStack = new Stack<E>();

	public E getMax()
	{
		return maxStack.peek();
	}

	public E push(E item)
	{
		stack.push(item);
		if (!maxStack.empty())
		{
			if (maxStack.peek().compareTo(item) < 0)
				maxStack.push(item);
		}
		else
			maxStack.push(item);
		return item;
	}

	public E pop()
	{
		E popped = stack.pop();
		if (!maxStack.empty())
		{
			if (maxStack.peek().equals(popped))
				maxStack.pop();
		}
		return popped;
	}

	public E peek()
	{
		return stack.peek();
	}
	
	public String toString()
	{
		return stack.toString();
	}

	public static void main(String args[])
	{
		MaxStack<Integer> m = new MaxStack<Integer>();
		
		m.push(new Integer(1));
		m.push(new Integer(4));
		m.push(new Integer(3));
		m.push(new Integer(5));
		System.out.println(m);
		System.out.println("max: " + m.getMax() + '\n');
		
		m.pop();
		System.out.println(m);
		System.out.println("max: " + m.getMax() + '\n');
		
		m.pop();
		System.out.println(m);
		System.out.println("max: " + m.getMax() + '\n');
		
		m.pop();
		System.out.println(m);
		System.out.println("max: " + m.getMax() + '\n');
		
		m.push(new Integer(18));
		m.push(new Integer(4));
		m.push(new Integer(16));
		m.push(new Integer(7));
		System.out.println(m);
		System.out.println("max: " + m.getMax() + '\n');
	}
}
