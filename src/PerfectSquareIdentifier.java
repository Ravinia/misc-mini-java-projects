import java.util.ArrayList;

public class PerfectSquareIdentifier
{
	public static boolean isPerfectSquare(int myInt)
	{
		double root = Math.sqrt(myInt);
		double floor = Math.floor(root);
		double ceil = Math.ceil(root);
		return floor == ceil;
	}
	
	public static void print(int doors)
	{
		ArrayList<Integer> perfectSquares = new ArrayList<Integer>();
		ArrayList<Integer> unperfectSquares = new ArrayList<Integer>();
		for (int i=1; i<=doors; i++)
		{
			if (isPerfectSquare(i))
				perfectSquares.add(i);
			else
				unperfectSquares.add(i);
		}
		System.out.println("perfectSquares: " + perfectSquares);
		System.out.println("unperfectSquares: " + unperfectSquares);
	}
	
	public static void main(String args[])
	{
		PerfectSquareIdentifier.print(100);
	}
}
