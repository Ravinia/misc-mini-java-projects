
public class Node
{
	public Node next;
	public Node prev;
	public String value;
	
	public Node(String value)
	{
		this.value = value;
	}
}
