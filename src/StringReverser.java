
public class StringReverser
{
	public static String reverseString(String s)
	{
		String r = "";
		for (char c: s.toCharArray())
			r = c + r;
		return r;
	}
	
	public static String reverseSentence(String s)
	{
		String r = "";
		for (String p: s.split(" "))
			r = p + " " + r;
		return r;
	}
	
	public static void main(String args[])
	{
		String s = "hey";
		System.out.println("before: " + s);
		String r = StringReverser.reverseString(s);
		System.out.println("after:  " + r + '\n');
		
		String sentence = "how was your day";
		System.out.println("before: " + sentence);
		String reversed = StringReverser.reverseSentence(sentence);
		System.out.println("after:  " + reversed + '\n');
	}
}
